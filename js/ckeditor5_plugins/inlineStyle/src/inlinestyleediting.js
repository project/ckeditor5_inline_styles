import { Plugin } from 'ckeditor5/src/core';
import InlineStyleCommand from "./inlinestylecommand";

export default class InlineStyleEditing extends Plugin {
  /**
   * @inheritdoc
   */
  static get pluginName() {
    return 'InlineStyleEditing';
  }

  /**
   * @inheritdoc
   */
  constructor(editor) {
    super(editor);

    this.attrs = {
      dataInlineStyle: 'data-inline-style',
    };
    this.converterAttributes = [
      'dataInlineStyle',
    ];
  }

  init() {
    const schema = this.editor.model.schema;
    const conversion = this.editor.conversion;

    if (schema.isRegistered('drupalMedia')) {
      schema.extend('drupalMedia', {
        allowAttributes: ['dataInlineStyle']
      });
    }

    // Set attributeToAttribute conversion for all supported attributes.
    Object.keys(this.attrs).forEach((modelKey) => {
      const attributeMapping = {
        model: {
          key: modelKey,
          name: 'drupalMedia',
        },
        view: {
          name: 'drupal-media',
          key: this.attrs[modelKey],
        },
      };
      conversion.for('upcast').attributeToAttribute(attributeMapping);
      conversion.for('downcast').attributeToAttribute(attributeMapping);
    });
    this.editor.commands.add(
      'addInlineStyle', new InlineStyleCommand( this.editor )
    );
  }
}
