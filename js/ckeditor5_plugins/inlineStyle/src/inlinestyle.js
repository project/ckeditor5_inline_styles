import { Plugin } from 'ckeditor5/src/core';
import InlineStyleEditing from "./inlinestyleediting";
import InlineStyleUI from "./inlinestyleui";

export default class InlineStyle extends Plugin {
  static get requires() {
    return [ InlineStyleEditing, InlineStyleUI ];
  }

  /**
   * @inheritdoc
   */
  static get pluginName() {
    return 'InlineStyle';
  }
}
