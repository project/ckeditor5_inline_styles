import { Plugin } from 'ckeditor5/src/core';
import { ButtonView, ContextualBalloon, clickOutsideHandler } from 'ckeditor5/src/ui';
import icon from '../../../../icons/padding-icon.svg';
import FormView from './inlinestyleview';
import { getInlineStyle } from './utils';

export default class InlineStyleUI extends Plugin {
  static get requires() {
    return [ ContextualBalloon ];
  }

  init() {
    const editor = this.editor;

    // Create the balloon and the form view.
    this._balloon = this.editor.plugins.get( ContextualBalloon );
    this.formView = this._createFormView();

    // This will register the simpleBox toolbar button.
    editor.ui.componentFactory.add( 'inlineStylePadding', (locale) => {
      const command = editor.commands.get('addInlineStyle');
      const button = new ButtonView(locale);
      button.label = 'Media Inline Style';
      button.icon = icon;
      button.tooltip = true;

      button.bind('isEnabled').to(command, 'isEnabled');

      // Show the UI on button click.
      this.listenTo( button, 'execute', () => {
        this._showUI();
      } );

      return button;
    } );
  }

  _createFormView() {
    const editor = this.editor;
    const formView = new FormView( editor.locale );

    // Execute the command after clicking the "Save" button.
    this.listenTo( formView, 'submit', () => {
      // Grab values from the padding input fields.
      const value = {
        padding: formView.paddingInputView.fieldView.element.value
      };
      editor.execute( 'addInlineStyle', value );

      // Hide the form view after submit.
      this._hideUI();
    } );

    // Hide the form view after clicking the "Cancel" button.
    this.listenTo( formView, 'cancel', () => {
      this._hideUI();
    } );

    // Hide the form view when clicking outside the balloon.
    clickOutsideHandler( {
      emitter: formView,
      activator: () => this._balloon.visibleView === formView,
      contextElements: [ this._balloon.view.element ],
      callback: () => this._hideUI()
    } );

    return formView;
  }

  _showUI() {
    // Check the value of the command.
    this._balloon.add( {
      view: this.formView,
      position: this._getBalloonPositionData()
    } );

    this.formView.paddingInputView.fieldView.value = getInlineStyle(this.editor);
    this.formView.focus();
  }

  _hideUI() {
    // Clear the input field values and reset the form.
    this.formView.paddingInputView.fieldView.value = getInlineStyle(this.editor);
    if (!this.formView.paddingInputView.fieldView.value) {
      this.formView.element.reset();
    }

    this._balloon.remove( this.formView );

    // Focus the editing view after inserting the abbreviation so the user can start typing the content
    // right away and keep the editor focused.
    this.editor.editing.view.focus();
  }

  _getBalloonPositionData() {
    const view = this.editor.editing.view;
    const viewDocument = view.document;
    let target = null;

    // Set a target position by converting view selection range to DOM
    target = () => view.domConverter.viewRangeToDom( viewDocument.selection.getFirstRange() );

    return {
      target
    };
  }
}
