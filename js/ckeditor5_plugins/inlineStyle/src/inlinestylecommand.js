import { Command } from 'ckeditor5/src/core';
import {
  getClosestSelectedDrupalMediaElement,
} from './utils.js';
export default class InlineStyleCommand extends Command {
  /**
   * The command value: `false` if there is no `alt` attribute, otherwise the value of the `alt` attribute.
   /**
   * @inheritdoc
   */
  refresh() {
    const drupalMediaElement = getClosestSelectedDrupalMediaElement(
      this.editor.model.document.selection
    );

    this.isEnabled =
      !!drupalMediaElement &&
      drupalMediaElement.getAttribute('drupalMediaEntityType') &&
      drupalMediaElement.getAttribute('drupalMediaEntityType') === 'media' &&
      drupalMediaElement.getAttribute('drupalMediaEntityType') !== 'METADATA_ERROR';
    if (this.isEnabled) {
      this.value = drupalMediaElement.getAttribute('dataInlineStyle');
    } else {
      this.value = false;
    }
  }

  /**
   * Executes the command.
   *
   * @param {Object} options
   *   An options object.
   * @param {String} options.newValue The new value of the `dataInlineStyle` attribute to set.
   */
  execute( options ) {
    const { model } = this.editor;
    const drupalMediaElement = getClosestSelectedDrupalMediaElement(
      model.document.selection
    );

    options.newValue = options.padding.trim();
    model.change((writer) => {
      if (options.newValue.length > 0) {
        writer.setAttribute(
          'dataInlineStyle',
          options.newValue,
          drupalMediaElement
        );
      } else {
        writer.removeAttribute('dataInlineStyle', drupalMediaElement);
      }
    });
  }
}
