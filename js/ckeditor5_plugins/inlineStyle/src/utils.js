/**
 * @license Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
 * For licensing, see LICENSE.md.
 */

/**
 * Checks if the provided model element is `drupalMedia`.
 *
 * @param {module:core/editor/editor~Editor} editor
 *   The model element to be checked.
 * @return {any[]}
 *   A string of inline styles.
 *
 * @private
 */
export function getInlineStyle(editor) {
  let inlinePadding = '';
  if (editor.getData()) {
    // Create a temporary element to parse the HTML string
    const tempElement = document.createElement('div');
    tempElement.innerHTML = editor.getData();

    // Find the element with the drupal-inline-padding attribute
    const drupalMediaElement = tempElement.querySelector('drupal-media');
    const dataInlineStyle = drupalMediaElement.getAttribute('data-inline-style');
    if (dataInlineStyle !== null) {
      inlinePadding = dataInlineStyle;
    }
    const parser = new DOMParser();
    const doc = parser.parseFromString(editor.getData(), 'text/html');
    const drupalMediaElements = doc.querySelectorAll('drupal-media');
    Array.from(drupalMediaElements).map(element => {
      const entityUUID = element.getAttribute('data-entity-uuid');
      const inlineStyle = element.getAttribute('data-inline-style');
      return { entityUUID, inlineStyle };
    });
  }
  return inlinePadding;
}

/**
 * Checks if the provided model element is `drupalMedia`.
 *
 * @param {module:engine/model/element~Element} modelElement
 *   The model element to be checked.
 * @return {boolean}
 *   A boolean indicating if the element is a drupalMedia element.
 *
 * @private
 */
export function isDrupalMedia(modelElement) {
	return !!modelElement && modelElement.is('element', 'drupalMedia');
}

/**
 * Gets `drupalMedia` element from selection.
 *
 * @param {module:engine/model/selection~Selection|module:engine/model/documentselection~DocumentSelection} selection
 *   The current selection.
 * @return {module:engine/model/element~Element|null}
 *   The `drupalMedia` element which could be either the current selected an
 *   ancestor of the selection. Returns null if the selection has no Drupal
 *   Media element.
 *
 * @private
 */
export function getClosestSelectedDrupalMediaElement(selection) {
	const selectedElement = selection.getSelectedElement();
	return isDrupalMedia(selectedElement) ? selectedElement : selection.getFirstPosition().findAncestor('drupalMedia');
}
