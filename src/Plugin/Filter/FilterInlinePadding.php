<?php

namespace Drupal\ckeditor5_inline_styles\Plugin\Filter;

use Drupal\Component\Utility\Html;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;

/**
 * Provides a filter to add inline style.
 *
 * @Filter(
 *   id = "filter_inline_style",
 *   title = @Translation("Inline Style"),
 *   description = @Translation("Uses a <code>data-inline-style</code> to add inline style."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_REVERSIBLE
 * )
 */
class FilterInlinePadding extends FilterBase {

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {
    $result = new FilterProcessResult($text);
    if (stristr($text, 'data-inline-style') !== FALSE) {
      $dom = Html::load($text);
      $xpath = new \DOMXPath($dom);
      foreach ($xpath->query('//*[@data-inline-style]') as $node) {
        // Read the drupal-inline-padding attribute's value, then delete it.
        $inline_padding = $node->getAttribute('data-inline-style');
        $node->removeAttribute('data-inline-style');
        // If one of the allowed alignments, add the corresponding class.
        if (!empty($inline_padding)) {
          $node->setAttribute('style', $inline_padding);
        }
      }
      $result->setProcessedText(Html::serialize($dom));
    }

    return $result;
  }

}
